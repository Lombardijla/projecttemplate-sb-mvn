package com.oreillyauto.dao.impl;

import java.sql.Timestamp;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.TestRepositoryCustom;
import com.oreillyauto.domain.Test;

@Repository
public class TestRepositoryImpl extends QuerydslRepositorySupport implements TestRepositoryCustom {
	
	public TestRepositoryImpl() {
		super(Test.class);
	}

	@Override
	public Timestamp getDatabaseTimestamp() {
	   Query query = getEntityManager().createNativeQuery("SELECT current_timestamp");
	   return (Timestamp)query.getSingleResult();
	}

}
